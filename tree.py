import numpy as np
import pandas as pd
from math import log


def entropy(ylist, possible_states):
    result = 0
    for i in range(possible_states):
        pi = ylist.count(i) / len(ylist)
        if pi == 0:
            continue
        result -= pi * log(pi, 2)
    return result


def gini(ylist, possible_states):
    result = 0
    for i in range(possible_states):
        result += (ylist.count(i) / len(ylist)) ** 2
    return 1 - result


def mse(ylist, possible_states):
    result = 0
    mean = sum(ylist) / len(ylist)
    for i in range(len(ylist)):
        result += (ylist[i] - mean) ** 2
    result /= len(ylist)
    return result


def mae(ylist, possible_states):
    result = 0
    mean = sum(ylist) / len(ylist)
    for i in range(len(ylist)):
        result += abs(ylist[i] - mean)
    result /= len(ylist)
    return result


def information_gain(x0, left_val, right_val, left_len, right_len):
    total_len = left_len + right_len
    return x0 - (left_len / total_len * left_val) - (right_len / total_len * right_val)


class Node:
    def __init__(self, attribute=None, separator=None, samples=None):
        self.left = None
        self.right = None

        self.attribute = attribute
        self.separator = separator
        self.samples = samples

        self.average = None
        self.classification_group = None


class DecisionTree:
    def __init__(self, criterion=gini, max_depth=float('+Inf'), min_samples_leaf=1):
        self.criterion = criterion
        self.max_depth = max_depth
        self.min_samples_leaf = min_samples_leaf
        self.trained_tree = None

    def fit(self, x, y):
        possible_states = len(y.unique())
        self.trained_tree = self.build_tree(x, y, possible_states)

    def build_tree(self, x, y, possible_states, current_depth=-1):
        attributes = x.columns.to_list()
        ylist = y.to_list()
        df = pd.concat([y, x], axis=1)

        x0 = self.criterion(ylist, possible_states)

        tree = Node()
        tree.samples = len(ylist)
        current_depth += 1

        if self.criterion == mse or self.criterion == mae:
            tree.average = sum(ylist) / len(ylist)
        else:
            tree.classification_group = max(set(ylist), key=ylist.count)

        if x0 == 0 or tree.samples <= self.min_samples_leaf or current_depth == self.max_depth:
            return tree
        else:
            best_ig = 0
            best_attribute = 0
            best_separator = 0
            best_dfs = (x, y)
            for attribute in attributes:
                df = df.sort_values(by=attribute)
                x = df[df.columns[1:]]
                y = df[df.columns[0]]

                xlist = x[attribute].to_list()
                ylist = y.to_list()

                for i in range(1, len(ylist)):
                    left_y = ylist[:i]
                    right_y = ylist[i:]
                    left_val = self.criterion(left_y, possible_states)
                    right_val = self.criterion(right_y, possible_states)

                    ig = information_gain(x0, left_val, right_val, len(left_y), len(right_y))

                    if ig > best_ig:
                        best_ig = ig
                        best_attribute = attribute
                        best_separator = (xlist[i - 1] + xlist[i]) / 2
                        best_dfs = (x, y)

            tree.attribute = best_attribute
            tree.separator = best_separator
            x, y = best_dfs

            left_x = x[x[tree.attribute] <= tree.separator]
            right_x = x[x[tree.attribute] > tree.separator]
            left_y = y.iloc[:left_x.shape[0]]
            right_y = y.iloc[left_x.shape[0]:]

            if len(right_y.to_list()) == 0 or len(left_y.to_list()) == 0:
                return

            tree.left = self.build_tree(left_x, left_y, possible_states, current_depth)
            tree.right = self.build_tree(right_x, right_y, possible_states, current_depth)
        return tree

    def predict(self, x):
        x = x.reset_index(drop=True)
        node = self.trained_tree
        answers = np.zeros(x.shape[0])
        answers = self.determine(x, node, answers)
        return answers

    def determine(self, df, node, answers):
        if node.left is None or node.right is None:
            for index in df.index.values:
                if node.average is None:
                    answers[index] = node.classification_group
                else:
                    answers[index] = node.average
            return answers

        left = df[df[node.attribute] <= node.separator]
        right = df[df[node.attribute] > node.separator]
        self.determine(left, node.left, answers)
        self.determine(right, node.right, answers)
        return answers