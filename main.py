from tree import DecisionTree, entropy, gini, mse, mae
from sklearn.tree import DecisionTreeClassifier as DecisionTreeClassifier
from sklearn.tree import DecisionTreeRegressor as DecisionTreeRegressor

import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_absolute_error


def main():
    df = pd.read_csv('https://stepik.org/media/attachments/course/57984/titanic.csv', index_col=0)
    df['Family'] = df.Parch + df.SibSp
    X = df[df.columns[1:]]
    Y = df['Survived']

    df2 = pd.read_csv('data_for_regression.csv', sep=';')
    X2 = df2[df2.columns[1:3]]
    Y2 = df2['sales']

    cols = ['Person', 'Family']
    X['Person'] = df.Sex
    X.loc[X['Sex'] == 'male', 'Person'] = 'Old male'
    X.loc[X['Sex'] == 'female', 'Person'] = 'Old female'
    X.loc[(X['Age'] < 50) & (X['Sex'] == 'male'), 'Person'] = 'Adult male'
    X.loc[(X['Age'] < 50) & (X['Sex'] == 'female'), 'Person'] = 'Adult female'
    X.loc[(X['Age'] < 15) & (X['Sex'] == 'male'), 'Person'] = 'Child male'
    X.loc[(X['Age'] < 15) & (X['Sex'] == 'female'), 'Person'] = 'Child female'

    X['Sex'] = LabelEncoder().fit_transform(X['Sex'])
    X['Person'] = LabelEncoder().fit_transform(X['Person'])

    X_train, X_test, Y_train, Y_test = train_test_split(X[cols], Y, test_size=0.2, random_state=0)

    mytreeC = DecisionTree()
    mytreeC.fit(X_train, Y_train)
    pred_mytreeC_train = mytreeC.predict(X_train)
    pred_mytreeC_test = mytreeC.predict(X_test)

    sktreeC = DecisionTreeClassifier()
    sktreeC.fit(X_train, Y_train)
    pred_sktree_train = sktreeC.predict(X_train)
    pred_sktree_test = sktreeC.predict(X_test)

    print('[Classification]')
    print("My tree:")
    print('Train accuracy score: ', np.round(accuracy_score(Y_train, pred_mytreeC_train), 4))
    print('Test accuracy score: ', np.round(accuracy_score(Y_test, pred_mytreeC_test), 4))
    print("\nsklearn tree:")
    print('Train accuracy score: ', np.round(accuracy_score(Y_train, pred_sktree_train), 4))
    print('Test accuracy score: ', np.round(accuracy_score(Y_test, pred_sktree_test), 4))

    X_train, X_test, Y_train, Y_test = train_test_split(X2, Y2, test_size=0.2, random_state=0)

    mytreeR = DecisionTree(criterion=mse)
    mytreeR.fit(X_train, Y_train)
    pred_mytreeR_train = mytreeR.predict(X_train)
    pred_mytreeR_test = mytreeR.predict(X_test)

    sktreeR = DecisionTreeRegressor()
    sktreeR.fit(X_train, Y_train)
    pred_sktreeR_train = sktreeR.predict(X_train)
    pred_sktreeR_test = sktreeR.predict(X_test)

    print('\n[Regression]')
    print("My tree:")
    print('Среднее абсолютное отклонение: ', np.round(mean_absolute_error(Y_train, pred_mytreeR_train), 2))
    print('Среднее абсолютное отклонение: ', np.round(mean_absolute_error(Y_test, pred_mytreeR_test), 2))
    print("\nsklearn tree:")
    print('Среднее абсолютное отклонение: ', np.round(mean_absolute_error(Y_train, pred_sktreeR_train), 2))
    print('Среднее абсолютное отклонение: ', np.round(mean_absolute_error(Y_test, pred_sktreeR_test), 2))


if __name__ == '__main__':
    main()